/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      transformOrigin: {
        'top-center': 'top center',
      },
      spacing: {
        0.25: '0.0625rem',
        0.75: '0.1875rem',
        25: '6.25rem',
      },
      animation: {
        wiggle: 'wiggle 5s ease-in-out infinite',
        steam: 'steam 3s ease-in-out infinite',
      },
      keyframes: {
        wiggle: {
          '0%, 100%': { transform: 'rotate(-3deg)' },
          '50%': { transform: 'rotate(3deg)' },
        },
        steam: {
          '0%': {
            'stroke-dashoffset': 9,
            opacity: 0,
          },
          '50%': {
            'stroke-dashoffset': 0,
            opacity: 0.6,
          },
          '100%': {
            'stroke-dashoffset': 39,
            opacity: 0,
          },
        },
      },
    },
  },
  plugins: [],
};
