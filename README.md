# App React todo list

## Key technologies

- vite
- react
- mobx
- typescript

## Functionality

- Registration, authorization and authentication
- Create, edit, delete tasks
- Drag and drop tasks

## Setup and Run

- npm install
- npm run dev

### .env example

```env
VITE_API_URL="http://localhost:5700/api"
```

### Dockerfile example

```dockerfile
FROM node:alpine

WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
CMD npm run dev
```
