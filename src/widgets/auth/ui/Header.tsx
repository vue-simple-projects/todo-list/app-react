import { Logo } from '@shared/ui';

export const Header = () => {
  return (
    <header className="flex flex-col items-center justify-between bg-fuchsia-400 text-white md:items-start">
      <Logo />
      <div className="w-full text-center text-xs font-light md:text-right md:text-base">
        <p>Simple application</p>
        <p>Simple life</p>
      </div>
    </header>
  );
};
