import { IAuthRequest, useAuth } from '@entities/auth';
import { Button, Input, LinkButton, LinkTo } from '@shared/ui';
import { useInput } from '@shared/libs';

export const LoginForm = () => {
  const authStore = useAuth();
  const email = useInput('');
  const password = useInput('');

  const login = () => {
    authStore.login({
      email: email.value,
      password: password.value,
    } as IAuthRequest);
  };

  // todo: need to add validation
  return (
    <>
      <header>
        <h3>Welcome back!</h3>
        <p>
          <span>{"Don't have an account?"}</span>
          <LinkTo to={'../registration'}>Sign up</LinkTo>
        </p>
      </header>
      <form>
        <fieldset>
          <Input
            id="email-field"
            autoComplete="on"
            onChange={email.onChange}
            value={email.value}
            type="text"
            placeholder="Email"
          />
          <Input
            id="password-field"
            autoComplete="on"
            onChange={password.onChange}
            value={password.value}
            type="password"
            placeholder="Password"
          />
        </fieldset>
        <footer>
          <Button onClick={login}>Login</Button>
          <LinkButton
            onClick={() => {
              // todo: need redirect to recovery password page
              console.log('redirect to recovery page');
            }}
          >
            Forgot Password?
          </LinkButton>
        </footer>
      </form>
    </>
  );
};
