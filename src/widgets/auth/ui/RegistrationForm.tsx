import { IAuthRequest, useAuth } from '@entities/auth';
import { Button, Input, LinkTo } from '@shared/ui';
import { useInput } from '@shared/libs';

export function RegistrationForm() {
  const userStore = useAuth();
  const email = useInput('');
  const password = useInput('');

  const register = () => {
    userStore.register({
      email: email.value,
      password: password.value,
    } as IAuthRequest);
  };

  // todo: need to add validation
  return (
    <>
      <header>
        <h3>Create account</h3>
        <p>
          <span>Already have an account?</span>
          <LinkTo to="../login">Login</LinkTo>
        </p>
      </header>
      <form>
        <fieldset>
          <Input
            id="email-field"
            autoComplete="on"
            onChange={email.onChange}
            value={email.value}
            type="text"
            placeholder="Email"
          />
          <Input
            id="password-field"
            autoComplete="on"
            onChange={password.onChange}
            value={password.value}
            type="password"
            placeholder="Password"
          />
        </fieldset>
        <footer>
          <Button onClick={register}>Create Account</Button>
        </footer>
      </form>
    </>
  );
}
