import { UnauthorizedGuard } from '@features/auth';
import { Header } from './Header';
import { Outlet } from 'react-router-dom';

export const Layout = () => {
  return (
    <>
      <UnauthorizedGuard>
        <section className="flex min-h-screen flex-nowrap items-center justify-center px-5 py-8 ">
          <div className="flex min-h-min flex-col overflow-hidden text-clip rounded-2xl md:flex-row [&>*]:px-8 [&>*]:py-6 md:[&>*]:p-16">
            <Header />
            <div className="relative flex flex-col gap-16 bg-white md:gap-24 [&_form]:flex [&_form]:flex-col [&_form]:gap-8  [&_form_fieldset]:flex [&_form_fieldset]:flex-col [&_form_fieldset]:gap-3 [&_form_footer]:flex [&_form_footer]:flex-col [&_form_footer]:gap-6 [&_header_h3]:text-2xl [&_header_h3]:font-semibold">
              <Outlet />
            </div>
          </div>
        </section>
      </UnauthorizedGuard>
    </>
  );
};
