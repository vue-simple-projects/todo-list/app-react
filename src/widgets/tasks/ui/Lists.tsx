import { observer } from 'mobx-react-lite';
import { useTaskStore } from '@entities/tasks';
import { TaskList } from '@widgets/tasks';

export const Lists = observer(function Lists() {
  const { lists } = useTaskStore();

  return (
    lists.length > 0 && (
      <div className="grid gap-10 font-medium md:grid-cols-1 lg:grid-cols-2 xl:grid-cols-3 2xl:grid-cols-4">
        {lists.map((l) => (
          <TaskList
            key={l.id}
            value={l}
          />
        ))}
      </div>
    )
  );
});
