import { Outlet } from 'react-router-dom';
import { AuthorizedGuard } from '@features/auth';
import { UserAvatar } from '@features/user';
import { Logo } from '@shared/ui';
import { AddTaskButton } from '@features/tasks';

export const Layout = () => {
  return (
    <AuthorizedGuard>
      <div className="flex flex-col  px-6 py-8">
        <header className="flex flex-row justify-between pb-6">
          <Logo />
          <div className="flex flex-row items-center gap-x-3">
            <AddTaskButton />
            <UserAvatar />
          </div>
        </header>
        <Outlet />
      </div>
    </AuthorizedGuard>
  );
};
