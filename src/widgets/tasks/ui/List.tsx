import { ITask } from '@entities/tasks';
import {
  TaskListHeader,
  ChildrenTasks,
  DragTaskWrapper,
} from '@features/tasks';

type Props = {
  value: ITask;
};

export const List = ({ value }: Props) => {
  return (
    <DragTaskWrapper
      task={value}
      className="group/list flex flex-col flex-nowrap gap-y-4 rounded-lg bg-white px-8 py-6"
      dragButtonClassName="-left-2 group-hover/list:opacity-100 opacity-0"
    >
      <TaskListHeader value={value} />
      <ChildrenTasks
        value={value.children}
        className="ml-0"
      />
    </DragTaskWrapper>
  );
};
