import { useUser } from '@entities/user';
import { observer } from 'mobx-react-lite';

export const Profile = observer(() => {
  const { user } = useUser();
  return (
    <ul className="[&_li]:flex [&_li]:flex-row [&_li_div:first-child]:text-slate-600 [&_li_div:last-child]:ml-2 ">
      <li>
        <div>email</div>
        <div>{user.email}</div>
      </li>
      <li>
        <div>activated</div>
        <div>{user.activated ? 'yes' : 'no'}</div>
      </li>
    </ul>
  );
});
