import { Outlet } from 'react-router-dom';
import { AuthorizedGuard } from '@features/auth/';
import { UserAvatar } from '@features/user';
import { Logo } from '@shared/ui';

export const Layout = () => {
  return (
    <AuthorizedGuard>
      <div className="flex flex-col  px-6 py-8">
        <header className="flex flex-row justify-between pb-6">
          <Logo />
          <div>
            <UserAvatar />
          </div>
        </header>
        <Outlet />
      </div>
    </AuthorizedGuard>
  );
};
