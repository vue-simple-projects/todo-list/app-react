import { makeAutoObservable } from 'mobx';
import { AuthApi, IAuthRequest } from '@entities/auth';
import { appConfig } from '@shared/libs';

export class Store {
  public authenticated: boolean = false;
  public token: string = '';

  constructor() {
    makeAutoObservable(this);
    const token = localStorage.getItem(appConfig.LOCAL_STORAGE_TOKEN_KEY) || '';
    this.setToken(token);
  }
  private setToken = (value: string) => {
    localStorage.setItem(appConfig.LOCAL_STORAGE_TOKEN_KEY, value);
    this.authenticated = !!value;
    this.token = value;
  };
  hasToken = (): boolean => {
    return !!localStorage.getItem(appConfig.LOCAL_STORAGE_TOKEN_KEY);
  };
  login = async (data: IAuthRequest) => {
    try {
      const response = await AuthApi.login(data);
      this.setToken(response.data.token);
    } catch (e) {
      console.error(e);
      this.setToken('');
    }
  };
  register = async (data: IAuthRequest) => {
    try {
      const response = await AuthApi.register(data);
      this.setToken(response.data.token);
    } catch (e) {
      console.error(e);
      this.setToken('');
    }
  };
  logout = async () => {
    try {
      await AuthApi.logout();
      localStorage.removeItem(appConfig.LOCAL_STORAGE_TOKEN_KEY);
    } catch (e) {
      console.error(e);
    } finally {
      this.setToken('');
    }
  };
  refresh = async () => {
    try {
      const response = await AuthApi.refreshToken();
      this.token = response.data.token;
      this.authenticated = true;
    } catch (e) {
      console.error('authStore.refresh', e);
      this.token = '';
    }
  };
}

export const store = new Store();
