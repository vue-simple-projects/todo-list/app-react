import type { AxiosResponse } from 'axios';
import { api, apiEndpoints } from '@shared/api';
import { IAuthRequest, IAuthResponse } from '@entities/auth';

export class Auth {
  static async login(
    data: IAuthRequest,
  ): Promise<AxiosResponse<IAuthResponse>> {
    return api.patch<IAuthResponse>(apiEndpoints.AUTH, data);
  }
  static async register(
    data: IAuthRequest,
  ): Promise<AxiosResponse<IAuthResponse>> {
    return api.post<IAuthResponse>(apiEndpoints.AUTH, data);
  }
  static async logout(): Promise<void> {
    await api.delete<IAuthResponse>(apiEndpoints.AUTH);
  }
  static async refreshToken(): Promise<AxiosResponse<IAuthResponse>> {
    const response = api.get<IAuthResponse>(apiEndpoints.AUTH);
    return response;
  }
}
