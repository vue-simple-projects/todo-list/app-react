import { createContext } from 'react';
import { AuthStore, authStore } from '@entities/auth';

export const AuthContext = createContext<AuthStore>(authStore);
