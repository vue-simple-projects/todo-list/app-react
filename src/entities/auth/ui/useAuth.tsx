import { useContext } from 'react';
import { AuthContext } from '@entities/auth';

export const useAuth = () => {
  return useContext(AuthContext);
};
