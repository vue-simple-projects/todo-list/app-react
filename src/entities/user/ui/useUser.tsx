import { useContext } from 'react';
import { UsersContext } from '.';

export const useUser = () => {
  const store = useContext(UsersContext);
  store.tryFetch();
  return store;
};
