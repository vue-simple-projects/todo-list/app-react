import { createContext } from 'react';
import { Store, store } from '../libs/Store';

export const UsersContext = createContext<Store>(store);
