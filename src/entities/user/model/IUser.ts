export interface IUser {
  id: string;
  email: string;
  activated: boolean;
}
