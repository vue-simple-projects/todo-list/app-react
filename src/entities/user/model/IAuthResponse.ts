import { IUser } from '@entities/user/model/IUser';

export interface IAuthResponse {
  token: string;
  user: IUser;
}
