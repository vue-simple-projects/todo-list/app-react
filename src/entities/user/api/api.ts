import type { AxiosResponse } from 'axios';
import { api, apiEndpoints } from '@shared/api';
import { IUser } from '@entities/user';

export class Api {
  static async getAll(): Promise<AxiosResponse<IUser[]>> {
    return api.get<IUser[]>(apiEndpoints.USERS);
  }
  static async getById(id: string): Promise<AxiosResponse<IUser>> {
    return api.get<IUser>(apiEndpoints.user(id));
  }
  static async getCurrent(): Promise<AxiosResponse<IUser>> {
    return api.get<IUser>(apiEndpoints.CURRENT_USER);
  }
  static async activate(activation: string): Promise<AxiosResponse<IUser>> {
    return api.post<IUser>(apiEndpoints.USER_ACTIVATION, activation);
  }
}
