import { makeAutoObservable } from 'mobx';
import { IUser, UserApi } from '@entities/user';

export class Store {
  public user = {} as IUser;
  private _fetched = false;

  constructor() {
    makeAutoObservable(this);
  }
  public tryFetch = async () => {
    if (this._fetched) {
      return;
    }
    this._fetched = true;

    try {
      const response = await UserApi.getCurrent();
      this.setUser(response.data);
    } catch (e) {
      console.error(e);
    }
  };
  private setUser = (user: IUser) => {
    this.user = user;
  };
}

export const store = new Store();
