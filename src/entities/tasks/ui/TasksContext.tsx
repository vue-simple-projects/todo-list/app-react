import { createContext } from 'react';
import { TasksStore, tasksStore } from '@entities/tasks';

export const TasksContext = createContext<TasksStore>(tasksStore);
