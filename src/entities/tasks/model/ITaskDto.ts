export interface ITaskDto {
  id: string;
  title: string;
  done: boolean;
  parent?: string | null;
  order?: number;
}
