import { ITaskDto } from '.';

export interface ITask extends ITaskDto {
  children: ITask[];
}
