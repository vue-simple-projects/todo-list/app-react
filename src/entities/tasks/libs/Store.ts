import { makeAutoObservable } from 'mobx';
import { ITask, ITaskDto, TasksApi } from '@entities/tasks';

export class Store {
  public tasks: ITask[] = [];
  private fetched: boolean = false;
  public lists: ITask[] = [];
  public focusedTaskId: string | null = null;

  public constructor() {
    makeAutoObservable(this);
  }
  public fetch = async () => {
    if (this.fetched) {
      return this.tasks;
    }
    this.setFetched(true);

    try {
      const response = await TasksApi.getAll();
      this.setTasks(response.data);
    } catch (e) {
      console.error(e);
    }
  };
  public setFetched = (value: boolean) => {
    this.fetched = value;
  };
  public setTasks(tasks: ITaskDto[]) {
    this.tasks = tasks as ITask[];
    this.tasks.sort((a, b) => (a.order ?? 0) - (b.order ?? 0));
    this.fillLists();
  }
  private fillLists = () => {
    this.tasks.forEach((t) => (t.children = this.getTasksByParentId(t.id)));
    this.lists = this.tasks.filter((t) => t.parent === null);
  };
  public getTasksByParentId = (parentId: string) => {
    return this.tasks.filter((t) => t.parent === parentId);
  };
  public update = async (task: ITask) => {
    try {
      await TasksApi.update(task);
      this.updateTask(task);
    } catch (e) {
      console.error(e);
    }
  };
  private updateTask = (task: ITask) => {
    const idx = this.tasks.findIndex((t) => t.id === task.id);
    this.tasks[idx] = task as ITask;
    this.fillLists();
  };
  public remove = async (task: ITask) => {
    try {
      await TasksApi.remove(task);
      this.removeTaskById(task);
    } catch (e) {
      console.error(e);
    }
  };
  private removeTaskById = (task: ITask) => {
    this.tasks = this.tasks.filter((t) => t.id !== task.id);
    this.fillLists();
  };
  public add = async (parent: string | null = null) => {
    const taskDto = {
      title: '',
      done: false,
      parent,
      order: this.tasks.length,
    } as ITaskDto;

    try {
      const response = await TasksApi.create(taskDto);
      const task = response.data as ITask;
      this.addTask(task);
      return task;
    } catch (e) {
      console.error(e);
    }
  };
  private addTask = (task: ITask) => {
    this.focusedTaskId = task.id;
    this.tasks.push(task);
    this.fillLists();
  };

  public resetFocusedTask = () => {
    this.focusedTaskId = null;
  };
  public getLastOrderByTask(task: ITask) {
    return task.children.length;
  }
}

export const store = new Store();
