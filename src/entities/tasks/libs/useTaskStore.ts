import { useContext } from 'react';
import { TasksContext } from '@entities/tasks';

export const useTaskStore = () => {
  const store = useContext(TasksContext);
  store.fetch();

  return store;
};
