import { api, apiEndpoints } from '@shared/api';
import { AxiosResponse } from 'axios';
import { ITaskDto } from '@entities/tasks/model';

export class TasksApi {
  static async getAll(): Promise<AxiosResponse<ITaskDto[]>> {
    return api.get<ITaskDto[]>(apiEndpoints.TASKS);
  }
  static async create(task: ITaskDto): Promise<AxiosResponse<ITaskDto>> {
    return api.post<ITaskDto>(apiEndpoints.TASKS, task);
  }
  static async update(task: ITaskDto): Promise<AxiosResponse<ITaskDto>> {
    return api.put<ITaskDto>(apiEndpoints.task(task.id), task);
  }
  static async remove(task: ITaskDto): Promise<AxiosResponse<ITaskDto[]>> {
    return api.delete<ITaskDto[]>(apiEndpoints.task(task.id));
  }
}
