import { Pages } from '@pages/index';
import { Providers } from './providers';

export const App = () => {
  return (
    <Providers>
      <Pages />
    </Providers>
  );
};
