import { ReactNode, Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { LoadingOverlay } from '@shared/ui';

type Props = {
  children: ReactNode;
};

export const Router = ({ children }: Props) => {
  return (
    <BrowserRouter>
      <Suspense fallback={<LoadingOverlay />}>{children}</Suspense>
    </BrowserRouter>
  );
};
