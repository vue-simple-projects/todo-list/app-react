import { ReactNode } from 'react';

type Props = {
  children: ReactNode;
};

export const Query = ({ children }: Props) => {
  return children;
};
