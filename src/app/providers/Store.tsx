import { ReactNode } from 'react';
import { userStore, UsersContext } from '@entities/user';
import { AuthContext, authStore } from '@entities/auth';
import { TasksContext, tasksStore } from '@entities/tasks';

type Props = {
  children: ReactNode;
};

export const Store = ({ children }: Props) => {
  return (
    <AuthContext.Provider value={authStore}>
      <UsersContext.Provider value={userStore}>
        <TasksContext.Provider value={tasksStore}>
          {children}
        </TasksContext.Provider>
      </UsersContext.Provider>
    </AuthContext.Provider>
  );
};
