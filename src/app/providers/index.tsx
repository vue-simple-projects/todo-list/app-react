import { Query } from './Query';
import { Router } from './Router';
import { ReactNode } from 'react';
import { Store } from './Store';

type Props = {
  children: ReactNode;
};

export const Providers = ({ children }: Props) => {
  return (
    <Query>
      <Store>
        <Router>{children}</Router>
      </Store>
    </Query>
  );
};
