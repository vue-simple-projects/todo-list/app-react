import { observer } from 'mobx-react-lite';
import { ReactNode, useContext, useEffect } from 'react';
import { AuthContext } from '@entities/auth';
import { useNavigate } from 'react-router-dom';
import { appEndpoints } from '@shared/libs';

type Props = {
  children: ReactNode;
};

export const AuthorizedGuard = observer(function AuthorizedGuard({
  children,
}: Props) {
  const { authenticated } = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (!authenticated)
      navigate(appEndpoints.LOGIN, {
        replace: true,
      });
  }, [authenticated, navigate]);

  return authenticated ? children : null;
});
