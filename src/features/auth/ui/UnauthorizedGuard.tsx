import { observer } from 'mobx-react-lite';
import { ReactNode, useContext, useEffect } from 'react';
import { AuthContext } from '@entities/auth';
import { appEndpoints } from '@shared/libs';
import { useNavigate } from 'react-router-dom';

type Props = {
  children: ReactNode;
};

export const UnauthorizedGuard = observer(function UnauthorizedGuard({
  children,
}: Props) {
  const { authenticated } = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (authenticated)
      navigate(appEndpoints.TASKS, {
        replace: true,
      });
  }, [authenticated, navigate]);

  return authenticated ? null : children;
});
