import { ITask } from '@entities/tasks';
import { Task } from './Task';
import { twMerge } from 'tailwind-merge';

type Props = {
  value: ITask[];
  className?: string;
};

export const ChildrenTasks = ({ value, className = 'ml-8' }: Props) => {
  const classes = twMerge(
    'flex flex-col flex-nowrap gap-x-4 gap-y-2',
    className,
  );

  return (
    <>
      {value.length > 0 && (
        <ul className={classes}>
          {value.map((t) => (
            <li key={t.id}>
              <Task value={t} />
            </li>
          ))}
        </ul>
      )}
    </>
  );
};
