import { ITask } from '@entities/tasks';
import { Checkbox, Text } from '@shared/ui';
import {
  useTask,
  useFocusTask,
  ChildrenTasks,
  DragTaskWrapper,
} from '@features/tasks';

type Props = {
  value: ITask;
};

export const Task = ({ value }: Props) => {
  const { updateTitle, updateDone, keyDownHandler } = useTask({
    value,
    newTaskParent: value.parent,
  });
  const { ref, trySetFocus } = useFocusTask<HTMLTextAreaElement>(value);
  trySetFocus();

  return (
    <DragTaskWrapper
      task={value}
      className="group/task-drag flex flex-col flex-nowrap gap-y-2 py-2 transition-transform"
      dragButtonClassName="h-5 group-hover/task-drag:opacity-100 opacity-0"
    >
      <div className="group/task flex flex-row flex-nowrap gap-x-3 rounded-md transition-transform">
        <Checkbox
          initialValue={value.done}
          onChange={updateDone}
        />
        <Text
          id={value.id}
          initialValue={value.title}
          ref={ref}
          onSpecialKeyDown={keyDownHandler}
          onBlur={updateTitle}
        />
      </div>
      {value.children.length > 0 && <ChildrenTasks value={value.children} />}
    </DragTaskWrapper>
  );
};
