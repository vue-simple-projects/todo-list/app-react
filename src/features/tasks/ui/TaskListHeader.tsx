import { Text } from '@shared/ui';
import { ITask } from '@entities/tasks';
import { AddTaskButton } from '.';
import { useTask } from '@features/tasks';

type Props = {
  value: ITask;
};

export const TaskListHeader = ({ value }: Props) => {
  const { updateTitle, keyDownHandler } = useTask({
    value,
    newTaskParent: value.id,
  });

  return (
    <header className="flex flex-row items-center gap-x-4 transition-transform">
      <Text
        id={value.id}
        initialValue={value.title}
        onBlur={updateTitle}
        onSpecialKeyDown={keyDownHandler}
        className="text-xl"
      />
      <AddTaskButton
        parent={value.id}
        className="[&_span]:bg-fuchsia-500"
      />
    </header>
  );
};
