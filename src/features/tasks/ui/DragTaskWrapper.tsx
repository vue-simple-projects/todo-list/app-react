import { ReactNode, useRef } from 'react';
import { twMerge } from 'tailwind-merge';
import { useDragTask } from '@features/tasks';
import { DragIcon } from '@shared/ui';
import { ITask } from '@entities/tasks';

type Props = {
  children: ReactNode;
  task: ITask;
  className?: string;
  dragButtonClassName?: string;
};

export const DragTaskWrapper = ({
  children,
  task,
  className = '',
  dragButtonClassName = '',
}: Props) => {
  const ref = useRef(null);
  const { isDragging, startDrag } = useDragTask(ref, task);
  const dragButtonClasses = twMerge(
    'absolute -left-8 h-6 w-8',
    dragButtonClassName,
  );
  const classes = twMerge('relative', className, isDragging && 'hidden');

  return (
    <section
      ref={ref}
      className={classes}
    >
      <button
        className={dragButtonClasses}
        onMouseDown={startDrag}
      >
        <DragIcon />
      </button>
      {children}
    </section>
  );
};
