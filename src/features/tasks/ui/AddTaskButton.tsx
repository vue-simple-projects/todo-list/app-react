import { useTaskStore } from '@entities/tasks';
import { AddIcon } from '@shared/ui';
import { twJoin } from 'tailwind-merge';

type Props = {
  parent?: string | null;
  className?: string;
};

export const AddTaskButton = ({ parent, className = '' }: Props) => {
  const { add } = useTaskStore();
  const classes = twJoin('h-6 w-6', className);

  return (
    <button
      className={classes}
      onClick={() => add(parent)}
    >
      <AddIcon />
    </button>
  );
};
