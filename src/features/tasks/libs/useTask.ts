import { ITask, useTaskStore } from '@entities/tasks';

type Props = {
  value: ITask;
  newTaskParent: string | null | undefined;
};

export const useTask = ({ value, newTaskParent }: Props) => {
  const { update, remove, add } = useTaskStore();

  const updateTitle = (title: string) => {
    if (title === '') {
      remove(value);
    } else if (title !== value.title) {
      update({ ...value, title });
    }
  };
  const updateDone = (done: boolean) => {
    if (value.done !== done) {
      update({ ...value, done });
    }
  };
  const keyDownHandler = async (key: string, title: string) => {
    if (key === 'Enter') {
      add(newTaskParent);
    }
    if (key === 'Backspace' && title === '') {
      remove(value);
    }
  };

  return { updateTitle, updateDone, keyDownHandler };
};
