import { RefObject, useCallback, useState } from 'react';
import { ITask, useTaskStore } from '@entities/tasks';

let draggingTask: ITask | null = null;

export const useDragTask = <T extends HTMLElement>(
  ref: RefObject<T>,
  task: ITask,
) => {
  const [isDragging, setDraggingState] = useState(false);
  const { update: updateTask, getLastOrderByTask } = useTaskStore();

  const init = useCallback(() => {
    if (!ref.current) {
      return;
    }

    ref.current.ondragover = (e) => {
      e.preventDefault();
      e.stopPropagation();
    };
    ref.current.ondragenter = (e) => {
      e.preventDefault();
      e.stopPropagation();
    };
    ref.current.ondragleave = (e) => {
      e.preventDefault();
      e.stopPropagation();
    };
    ref.current.ondragend = (e) => {
      e.preventDefault();
      e.stopPropagation();
      setDraggingState(false);

      if (ref.current) {
        ref.current.removeAttribute('draggable');
        ref.current.ondragstart = () => {};
      }
    };
    ref.current.ondrop = (e) => {
      e.preventDefault();
      e.stopPropagation();
      if (!draggingTask) {
        return;
      }

      const t = {
        ...draggingTask,
        parent: task.id,
        order: getLastOrderByTask(task),
      };
      updateTask(t);
      draggingTask = null;
    };
  }, [getLastOrderByTask, ref, task, updateTask]);

  const startDrag = useCallback(() => {
    if (!ref.current) {
      return;
    }
    draggingTask = task;
    if (ref.current) {
      ref.current.setAttribute('draggable', 'true');
      ref.current.ondragstart = () => {
        setTimeout(() => {
          setDraggingState(true);
        }, 1);
      };
    }
  }, [ref, task]);

  setTimeout(() => {
    init();
  }, 1);
  return { isDragging, startDrag };
};
