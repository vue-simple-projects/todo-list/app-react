import { ITask, useTaskStore } from '@entities/tasks';
import { useFocus } from '@shared/libs';

export const useFocusTask = <T extends HTMLElement>(task: ITask) => {
  const { focusedTaskId, resetFocusedTask } = useTaskStore();
  const { ref, setFocus } = useFocus<T>();

  const trySetFocus = () => {
    setTimeout(() => {
      if (focusedTaskId === task.id) {
        setFocus();
        resetFocusedTask();
      }
    }, 0);
  };

  return { ref, trySetFocus };
};
