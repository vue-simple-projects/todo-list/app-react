import { useNavigate } from 'react-router-dom';
import { useAuth } from '@entities/auth';
import { appEndpoints } from '@shared/libs';
import { Dropdown, UserIcon } from '@shared/ui';
import { observer } from 'mobx-react-lite';

export const Avatar = observer(() => {
  const navigate = useNavigate();
  const { logout } = useAuth();

  const list = [
    {
      title: 'profile',
      action: () => {
        navigate(appEndpoints.PROFILE);
      },
    },
    {
      title: 'tasks',
      action: () => {
        navigate(appEndpoints.TASKS);
      },
    },
    {
      title: 'logout',
      action: () => {
        logout();
      },
    },
  ];
  return (
    <Dropdown
      buttonView={<UserIcon className="h-6 w-6" />}
      list={list}
    />
  );
});
