import { Navigate, Route, Routes } from 'react-router-dom';
import { Suspense, lazy } from 'react';
import { AuthLayout } from '@widgets/auth';
import { appEndpoints } from '@shared/libs';
import { LoadingOverlay } from '@shared/ui';

const LoginForm = lazy(() =>
  import('@widgets/auth').then((module) => ({
    default: module.LoginForm,
  })),
);
const RegistrationForm = lazy(() =>
  import('@widgets/auth').then((module) => ({
    default: module.RegistrationForm,
  })),
);

export const AuthPage = () => {
  return (
    <Routes>
      <Route
        path="/"
        element={<AuthLayout />}
      >
        <Route
          path={appEndpoints.RELATIVE_LOGIN}
          element={
            <Suspense fallback={<LoadingOverlay />}>
              <LoginForm />
            </Suspense>
          }
        />
        <Route
          path={appEndpoints.RELATIVE_REGISTRATION}
          element={
            <Suspense fallback={<LoadingOverlay />}>
              <RegistrationForm />
            </Suspense>
          }
        />
        <Route
          path="*"
          element={
            <Navigate
              replace
              to={appEndpoints.REGISTRATION}
            />
          }
        />
      </Route>
    </Routes>
  );
};
