import { Route, Routes } from 'react-router-dom';
import { useTaskStore } from '@entities/tasks';
import { TaskLayout, TaskLists } from '@widgets/tasks';

export const TasksPage = () => {
  const { fetch } = useTaskStore();

  return (
    <>
      <Routes>
        <Route
          path="/"
          element={<TaskLayout />}
        >
          <Route
            index
            element={<TaskLists />}
            loader={async () => {
              return await fetch();
            }}
          />
        </Route>
      </Routes>
    </>
  );
};
