import { Navigate, Route, Routes } from 'react-router-dom';
import { appEndpoints } from '@shared/libs';
import { Suspense, lazy } from 'react';
import { LoadingOverlay } from '@shared/ui';

const TasksPage = lazy(() =>
  import('./TasksPage').then((module) => ({
    default: module.TasksPage,
  })),
);

const AuthPage = lazy(() =>
  import('./AuthPage').then((module) => ({
    default: module.AuthPage,
  })),
);

const ProfilePage = lazy(() =>
  import('./ProfilePage').then((module) => ({
    default: module.ProfilePage,
  })),
);

export const Pages = () => {
  return (
    <Routes>
      <Route
        path="*"
        element={
          <Navigate
            to={appEndpoints.TASKS}
            replace
          />
        }
      />
      <Route
        path={`${appEndpoints.TASKS}/*`}
        element={<TasksPage />}
      />
      <Route
        path={`${appEndpoints.AUTH}/*`}
        element={
          <Suspense fallback={<LoadingOverlay />}>
            <AuthPage />
          </Suspense>
        }
      />
      <Route
        path={`${appEndpoints.PROFILE}/*`}
        element={
          <Suspense fallback={<LoadingOverlay />}>
            <ProfilePage />
          </Suspense>
        }
      />
    </Routes>
  );
};
