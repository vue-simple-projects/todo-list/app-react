import { Navigate, Route, Routes } from 'react-router-dom';
import { UserLayout, UserProfile } from '@widgets/user';
import { appEndpoints } from '@shared/libs';

export const ProfilePage = () => {
  return (
    <>
      <Routes>
        <Route
          path="/"
          element={<UserLayout />}
        >
          <Route
            index
            element={<UserProfile />}
          />
        </Route>
        <Route
          path="*"
          element={
            <Navigate
              replace
              to={appEndpoints.PROFILE}
            />
          }
        />
      </Routes>
    </>
  );
};
