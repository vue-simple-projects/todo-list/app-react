export const endpoints = Object.freeze({
  AUTH: '/auth',

  TASKS: '/tasks',
  task: (id: string) => `/tasks/${id}`,

  USERS: '/users',
  CURRENT_USER: '/users/by-token',
  USER_ACTIVATION: '/users/activation',
  user: (id: string) => `/users/${id}`,
});
