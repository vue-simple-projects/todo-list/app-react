import axios from 'axios';
import { config } from './config';
import { AxiosInterceptors } from './interseptors';

export const api = axios.create({
  withCredentials: true,
  baseURL: config.API_URL,
  headers: config.API_HEADERS,
});

AxiosInterceptors.setupInterceptorsTo(api);
