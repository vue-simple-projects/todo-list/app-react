import axios, {
  AxiosError,
  AxiosInstance,
  InternalAxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import { config as apiConfig } from './config';
import { appConfig } from '@shared/libs';
import { api } from './api';

export class AxiosInterceptors {
  static setupInterceptorsTo(axiosInstance: AxiosInstance): AxiosInstance {
    axiosInstance.interceptors.request.use(
      AxiosInterceptors.onRequest,
      AxiosInterceptors.onRequestError,
    );
    axiosInstance.interceptors.response.use(
      AxiosInterceptors.onResponse,
      AxiosInterceptors.onResponseError,
    );
    return axiosInstance;
  }

  private static onRequest(
    config: InternalAxiosRequestConfig,
  ): InternalAxiosRequestConfig {
    AxiosInterceptors.trySetAuthorizationHeader(config);
    return config;
  }

  static trySetAuthorizationHeader(config: InternalAxiosRequestConfig) {
    const token = localStorage.getItem(appConfig.LOCAL_STORAGE_TOKEN_KEY);
    config.headers.Authorization = `Bearer ${token}`;
  }

  private static onRequestError(error: AxiosError): Promise<AxiosError> {
    return Promise.reject(error);
  }

  private static onResponse(response: AxiosResponse): AxiosResponse {
    return response;
  }

  private static async onResponseError(error: AxiosError): Promise<AxiosError> {
    if (error?.response?.status === 401) {
      try {
        const response = await AxiosInterceptors.tryRefreshToken();
        AxiosInterceptors.setToken(response.data.token);

        // return Promise.reject(error);
        const config = error.config;
        if (config) {
          config.headers.Authorization = `Bearer ${response.data.token}`;
          return api(config);
        }

        return Promise.reject(error);
      } catch (e) {
        AxiosInterceptors.removeToken();
        return Promise.reject(e);
      }

      return Promise.reject(error);
    }

    return Promise.reject(error);
  }

  private static async tryRefreshToken() {
    const api = axios.create({
      withCredentials: true,
      baseURL: apiConfig.API_URL,
      headers: apiConfig.API_HEADERS,
    });
    return await api.get('/auth');
  }

  private static setToken(token: string): void {
    window.localStorage.setItem(appConfig.LOCAL_STORAGE_TOKEN_KEY, token);
  }

  private static removeToken(): void {
    window.localStorage.removeItem(appConfig.LOCAL_STORAGE_TOKEN_KEY);
  }
}
