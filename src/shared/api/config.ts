export const config = Object.freeze({
  API_URL: import.meta.env.VITE_API_URL || 'http://localhost:5700/api',
  API_HEADERS: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});
