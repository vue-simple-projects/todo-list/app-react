export const AddIcon = () => {
  return (
    <div className="group/add-button relative h-full w-full transition-transform hover:scale-125 active:scale-90 [&>*]:absolute [&>*]:rounded-sm [&>*]:bg-slate-300">
      <div className="inset-x-0 mx-auto h-full w-0.75 rotate-90 group-hover/add-button:bg-fuchsia-500" />
      <div className="inset-x-0 mx-auto h-full w-0.75 group-hover/add-button:bg-fuchsia-500" />
    </div>
  );
};
