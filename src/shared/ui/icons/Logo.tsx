export const Logo = () => {
  return (
    <div className="font-mono">
      <strong className="text-lg font-thin text-slate-600">todo</strong>
      <span className="text-sm font-black">list</span>
    </div>
  );
};
