export const DragIcon = () => {
  return (
    <div className="group grid cursor-move grid-cols-2 place-content-center gap-x-0.5 gap-y-0.25 rounded py-1 transition-all hover:scale-125 [&>span:nth-child(2n+1)]:place-self-end [&>span]:h-1 [&>span]:w-1 [&>span]:rounded-full [&>span]:bg-slate-200">
      <span />
      <span className="group-hover:bg-fuchsia-500" />
      <span className="group-hover:bg-fuchsia-500" />
      <span />
      <span />
      <span className="group-hover:bg-fuchsia-500" />
    </div>
  );
};
