import { MouseEventHandler, ReactNode, MouseEvent } from 'react';

type Props = {
  children: ReactNode;
  onClick: MouseEventHandler<HTMLElement>;
};

export const LinkButton = ({ children, onClick, ...props }: Props) => {
  const handleClickEvent = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    onClick(e);
  };

  return (
    <button
      onClick={handleClickEvent}
      className="w-full text-center text-fuchsia-400 transition-colors hover:text-fuchsia-500"
      {...props}
    >
      {children}
    </button>
  );
};
