import { ChangeEvent, memo, useState } from 'react';

type Props = {
  initialValue: boolean;
  onChange: (checked: boolean) => void;
};

export const Checkbox = memo(function Checkbox({
  initialValue,
  onChange,
  ...props
}: Props) {
  const [done, setDone] = useState(initialValue);
  const handleChangeEvent = (e: ChangeEvent<HTMLInputElement>) => {
    setDone(e.target.checked);
    onChange(e.target.checked);
  };

  return (
    <input
      type="checkbox"
      checked={done}
      onChange={handleChangeEvent}
      className="relative h-5 w-5 cursor-pointer transition-transform before:absolute before:h-full before:w-full before:rounded before:border before:border-white before:bg-fuchsia-50 before:outline before:outline-1 before:outline-fuchsia-100 before:transition-colors before:content-[''] checked:before:bg-fuchsia-500 hover:scale-110 hover:before:bg-fuchsia-100 checked:hover:scale-90 checked:hover:before:bg-fuchsia-300 active:scale-125 checked:active:scale-75 group-hover/task:[&:not(:focus-visible)]:before:outline-fuchsia-300"
      {...props}
    />
  );
});
