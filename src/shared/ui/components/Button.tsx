import { MouseEventHandler, ReactNode, MouseEvent } from 'react';

type Props = {
  children: ReactNode;
  onClick: MouseEventHandler<HTMLElement>;
};

export const Button = ({ children, onClick, ...props }: Props) => {
  const handleClickEvent = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    onClick(e);
  };

  return (
    <button
      onClick={handleClickEvent}
      className="w-full rounded-lg bg-fuchsia-400  px-5 py-4 font-semibold text-white antialiased transition-all hover:bg-fuchsia-500"
      {...props}
    >
      {children}
    </button>
  );
};
