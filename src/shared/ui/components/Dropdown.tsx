import { MouseEvent, ReactNode, useState } from 'react';
import { twJoin } from 'tailwind-merge';

type Elem = {
  title: string;
  action: () => void;
};

type Props = {
  buttonView: ReactNode;
  list: Elem[];
};
export const Dropdown = ({ buttonView, list }: Props) => {
  const [visible, setVisible] = useState(false);
  const toggleVisible = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    setVisible(!visible);
  };

  const buttonClassName = twJoin(
    'absolute right-0 top-1 w-max rounded-md bg-white px-3 py-4 transition-all origin-top-right z-10',
    !visible && 'opacity-0 scale-50 pointer-events-none',
  );
  const callAction = (e: MouseEvent<HTMLElement>, action: () => void) => {
    e.preventDefault();
    action();
  };

  return (
    <div>
      <button onClick={(e) => toggleVisible(e)}>{buttonView}</button>
      <div className="relative antialiased">
        <ul className={buttonClassName}>
          {list.map((elem) => {
            return (
              <li
                key={elem.title}
                className="w-full px-2 py-1 text-fuchsia-400  transition-transform hover:scale-110 hover:text-fuchsia-500"
              >
                <button
                  onClick={(e: MouseEvent<HTMLElement>) =>
                    callAction(e, elem.action)
                  }
                >
                  {elem.title}
                </button>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};
