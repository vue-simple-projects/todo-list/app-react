import { LoadingIcon } from '@shared/ui';

export const LoadingOverlay = () => {
  return (
    <div className="absolute inset-0 flex items-center justify-center">
      <LoadingIcon />
    </div>
  );
};
