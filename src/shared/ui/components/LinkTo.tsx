import { ReactNode } from 'react';
import { Link } from 'react-router-dom';
type Props = {
  children: ReactNode;
  to: string;
};

export const LinkTo = ({ children, to, ...props }: Props) => {
  return (
    <Link
      to={to}
      className="ml-2 text-fuchsia-400 transition-colors hover:text-fuchsia-500"
      {...props}
    >
      {children}
    </Link>
  );
};
