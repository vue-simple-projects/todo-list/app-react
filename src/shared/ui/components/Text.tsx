import { useForwardRef } from '@shared/libs';
import {
  ChangeEvent,
  FocusEvent,
  KeyboardEvent,
  forwardRef,
  memo,
  useCallback,
  useEffect,
  useState,
} from 'react';
import { twMerge } from 'tailwind-merge';

type Props = {
  id: string;
  initialValue: string;
  onBlur: (value: string) => void;
  onSpecialKeyDown?: (key: string, value: string) => void;
  onChange?: (value: string) => void;
  className?: string;
};

const Txt = forwardRef<HTMLTextAreaElement, Props>(function Text(
  { id, initialValue, onBlur, className, onSpecialKeyDown }: Props,
  ref,
) {
  const textRef = useForwardRef<HTMLTextAreaElement>(ref);
  const [text, setText] = useState(initialValue);
  const classes = twMerge(
    'flex-1 border-b border-slate-100 text-sm font-normal leading-5 text-slate-800 antialiased outline-none transition-colors focus-visible:border-fuchsia-500 focus-visible:outline-0 group-hover/task:[&:not(:focus-visible)]:border-fuchsia-300 resize-none pb-1',
    className,
  );
  const autoSize = useCallback(() => {
    if (textRef && textRef.current) {
      textRef.current.style.height = textRef.current.scrollHeight + 'px';
    }
  }, [textRef]);
  const changeHandler = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setText(e.target.value);
    autoSize();
  };
  const blurHandler = (e: FocusEvent<HTMLTextAreaElement>) => {
    e.preventDefault();
    onBlur(e.target.value);
  };
  const keyDownHandler = (e: KeyboardEvent<HTMLTextAreaElement>) => {
    if (!onSpecialKeyDown) {
      return;
    }

    if (e.code === 'Enter') {
      e.preventDefault();
      onSpecialKeyDown('Enter', (e.target as HTMLTextAreaElement).value);
    } else if (e.code === 'Backspace') {
      onSpecialKeyDown('Backspace', (e.target as HTMLTextAreaElement).value);
    } else if (e.code === 'Tab') {
      onSpecialKeyDown('Tab', (e.target as HTMLTextAreaElement).value);
    }
  };

  useEffect(() => {
    autoSize();
  }, [autoSize]);

  return (
    <textarea
      id={id}
      ref={textRef}
      className={classes}
      onBlur={blurHandler}
      onKeyDown={keyDownHandler}
      onChange={changeHandler}
      placeholder="Task"
      rows={1}
      autoComplete="on"
      spellCheck
      wrap="hard"
      value={text}
    />
  );
});

export const Text = memo(Txt);
