import { ChangeEventHandler } from 'react';

type Props = {
  placeholder?: string;
  autoComplete?: 'on' | undefined;
  onChange: ChangeEventHandler;
  value: string;
  type: 'text' | 'password';
  id: string;
};

export const Input = ({ placeholder, id, ...props }: Props) => {
  return (
    <p className="relative">
      <input
        id={id}
        {...props}
        placeholder={placeholder}
        className="peer w-full border-b border-fuchsia-50 pt-4 text-lg font-normal antialiased caret-fuchsia-500 outline-none transition-colors placeholder:text-transparent hover:border-fuchsia-300 focus-visible:border-fuchsia-500   "
      />
      <label
        htmlFor={id}
        className="pointer-events-none absolute -top-1 left-0 text-sm font-normal text-slate-500 transition-all peer-placeholder-shown:top-4 peer-placeholder-shown:text-lg"
      >
        {placeholder}
      </label>
    </p>
  );
};
