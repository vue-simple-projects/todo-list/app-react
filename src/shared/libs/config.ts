export const config = Object.freeze({
  LOCAL_STORAGE_TOKEN_KEY:
    import.meta.env.LOCAL_STORAGE_TOKEN_NAME || 'todo-list.access.token',
});
