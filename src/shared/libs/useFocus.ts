import { useRef } from 'react';

export const useFocus = <T extends HTMLElement>() => {
  const ref = useRef<T>(null);
  const setFocus = () => {
    setTimeout(() => {
      if (ref.current) {
        ref.current.focus();
      }
    }, 0);
  };

  return { ref, setFocus };
};
